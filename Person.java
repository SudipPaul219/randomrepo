public class Person{

    String firstName;
    String lastName;

     public String toString(){
         return firstName + " " + lastName;
     }


}

class Student extends Person {

    double gpa;

    public String toString() {
        return gpa + "";
    }


}

class Test{

    public static void main(String[] args) {

        Person p = new Person();
        Student s = new Student();

        p = new Student();
        //s = new Person();

        p = (Person) new Student();
        p = (Student) new Student();

        //s = (Person) new Person();
        s = (Student) new Person();
    }




}
