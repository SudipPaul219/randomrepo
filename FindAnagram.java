import java.util.HashMap;

public class FindAnagram {

    public static void main(String[] args) {

        String str1 = "geeksforgeeks";
        String str2 = "helloworld";

        System.out.println(str1+" and "+str2+" are anagrams? : "+ areAnagram(str1,str2));

    }

    public static boolean areAnagram(String str1, String str2){

        HashMap<String, Integer> hmap1 = new HashMap<>();
        HashMap<String, Integer> hmap2 = new HashMap<>();

        char[] arr1 = str1.toCharArray();
        char[] arr2 = str2.toCharArray();

        for(int i = 0; i < arr1.length; i++){

            if(hmap1.get(arr1[i]) == null){

                // PUT THE CHAR INTO THE HASHMAP
                hmap1.put(Character.toString(arr1[i]),1);
            }
            else{
                // NOW WE KNOW THERE IS ALREADY A CHAR IN THE MAP
                int c = (int) hmap1.get(Character.toString(arr1[i]));
                hmap1.put(Character.toString(arr1[i]), ++c);
            }

        }

        for (int j = 0; j < arr2.length; j++) {

            if (hmap2.get(arr2[j]) == null)
                hmap2.put(Character.toString(arr2[j]), 1);
            else {

                Integer d = (int)hmap2.get(arr2[j]);
                hmap2.put(Character.toString(arr2[j]), ++d);
            }
        }

        if(hmap1.equals(hmap2)){
            return true;
        }
        else{

            return false;
        }


    }
}
