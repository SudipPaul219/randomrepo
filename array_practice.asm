.data 

array: .word 13 11 9 8 5 6 1 2

.text

la $s0, array
li $t0, 0	# count
li $t1, 0	# will hold word
	
loop:
	beq $t0, 8, done	# end condition
	
	lw $t1, 0($s0)		# load the word
		
	move $a0, $t1		# print the word
	li $v0, 1
	syscall
	
	li $a0, '\n'
	li $v0, 11
	syscall
	
	addi $t0, $t0, 1	# count++
	addi $s0, $s0, 4	# next array index
	
	j loop
	
done:
	# restore array index
	
	li $t2, 4
	mul $t3, $t2, $t0	# count * 4 (use to subtract from the array address)
	sub $s1, $s1, $t3	# restore index
	
	li $v0, 10
	syscall
